const Client = require('../models/client.model.js');

// Add a new Client
exports.create = (req, res) => {

    console.log(req)
    // Validate request
    if(!req.body.email) {
        return res.status(400).send({
            message: "Client email can not be empty"
        });
    }

    // Add a Client
    const client = new Client({
        name: req.body.name || "Anonymous", 
        email: req.body.email,
        createdDate: new Date(),
        companyFields: req.body.companyFields
    });

    // Save Client in the database
    client.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all clients from the database.
exports.findAll = (req, res) => {
    Client.find()
    .then(client => {
        res.send(client);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the list of client."
        });
    });
};

// Find a single client with a clientId
exports.findOne = (req, res) => {
    Client.findById(req.params.clientId)
    .then(client => {
        if(!client) {
            return res.status(404).send({
                message: "Client not found with id " + req.params.clientId
            });            
        }
        res.send(client);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Client not found with id " + req.params.clientId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.clientId
        });
    });
};

// Update a client identified by the clientId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.email) {
        return res.status(400).send({
            message: "Email cannot be empty"
        });
    }

    // Find client and update it with the request body
    Client.findByIdAndUpdate(req.params.clientId, {
        name: req.body.name || "Untitled Note",
        content: req.body.email,
        companyFields: req.body.companyFields
    }, {new: true})
    .then(client => {
        if(!client) {
            return res.status(404).send({
                message: "Client not found with id " + req.params.clientId
            });
        }
        res.send(client);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Client not found with id " + req.params.clientId
            });                
        }
        return res.status(500).send({
            message: "Error updating client with id " + req.params.clientId
        });
    });
};

// Delete a client with the specified clientId in the request
exports.delete = (req, res) => {
    Client.findByIdAndRemove(req.params.clientId)
    .then(client => {
        if(!client) {
            return res.status(404).send({
                message: "Client not found with the id " + req.params.clientId
            });
        }
        res.send({message: "Client deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Client not found with the id " + req.params.clientId
            });                
        }
        return res.status(500).send({
            message: "Could not delete client with the id " + req.params.clientId
        });
    });
};
