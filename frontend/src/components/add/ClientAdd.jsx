import React, { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import { createClient } from '../../utils'

export default function ClientAdd() {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("sss");
  const [companyField, setCompanyField] = useState("");
  const [message, setMessage] = useState("");

  let handleSubmit = async (e) => {
    e.preventDefault();
    let body = {
      name: name,
      email: email,
      companyFields: companyField,
      createdAt: new Date(),
    }
    try {
      let res = await createClient(body)
      .then((res) => res.json());
      console.log(res._id)
      if (res._id) {
        setMessage(`Client ${res.name} has been added to the database with the id ${res._id}`);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
      position="static"
      color="default"
      elevation={0}
      sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
            <Grid container spacing={3} alignItems="center">
            </Grid>
        </Toolbar>
      </AppBar>
      <Typography sx={{ my: 5, mx: 2 }} color="text.secondary" align="center">
          <p>{message}</p>
      </Typography>
      <Grid container>
        <form onSubmit={handleSubmit}>
          <TextField
              style={{ width: "200px", margin: "5px" }}
              value={name}
              type="text"
              label="name"
              variant="outlined"
              onChange={(e) => setName(e.target.value)}
            />
          <TextField
            style={{ width: "200px", margin: "5px" }}
            value={email}
            type="email"
            label="email"
            variant="outlined"
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            style={{ width: "200px", margin: "5px" }}
            value={new Date()}
            type="text"
            label="created At"
            variant="outlined"
            disabled
          />
          <TextField
            style={{ width: "200px", margin: "5px" }}
            value={companyField}
            type="text"
            label="company fields"
            variant="outlined"
            onChange={(e) => setCompanyField(e.target.value)}
          />
          <Button type="submit">
            SEND
          </Button>
          </form>
        </Grid>
    </Paper>
  );
}