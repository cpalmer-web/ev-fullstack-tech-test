const mongoose = require('mongoose');

const ClientSchema = mongoose.Schema({
    name: String,
    email: String,
    createdDate: Date,
    companyFields: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Client', ClientSchema);