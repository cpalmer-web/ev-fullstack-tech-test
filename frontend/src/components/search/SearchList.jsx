import React from 'react';
import ClientCard from '../ClientCard';
import Grid from '@material-ui/core/Grid'

function SearchList({ filteredClients }) {
  const filtered = filteredClients.map(client =>  (<Grid key={client._id} item xs><ClientCard client={client} /></Grid>)); 
  return (
    <>
     {filtered}
    </>
  );
}

export default SearchList;