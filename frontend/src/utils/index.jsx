import fetch from 'isomorphic-fetch'
import { API_URL } from './constants'

export function createClient(body) {
    const data = fetch(`${API_URL}clients/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body),
      });
    return data;
}

export function removeClient(id) {
    const data = fetch(`${API_URL}clients/${id}`, { method: 'DELETE' })
    return data;
}

export function getAllClients() {
    const data = fetch(`${API_URL}clients/`)
    return data;
}