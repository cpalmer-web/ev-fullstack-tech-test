import React, { useState, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import { getAllClients } from '../../utils'
import SearchList from './SearchList';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import RefreshIcon from '@mui/icons-material/Refresh';

export default function ClientSearch () {
  
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [clients, setClients] = useState([]);
  const [searchField, setSearchField] = useState("");
  const [val, setVal] = useState();

    useEffect(() => {
      getAllClients()
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);
            setClients(result);
          },
          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )
    }, [clients])

    const filteredClients = clients.filter(
      client => {
        return (
          client
          .name
          .toLowerCase()
          .includes(searchField.toLowerCase()) ||
          client
          .email
          .toLowerCase()
          .includes(searchField.toLowerCase())
        );
      }
    );

    const handleChange = e => {
      setSearchField(e.target.value);
    };
  
    const clearSearch = () => {
      setSearchField(""),
      setVal(() => "")
    }

    function searchList() {
      return (
        <Grid container spacing={2} alignItems="center">
          <SearchList filteredClients={filteredClients} />
        </Grid>
      );
    }

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div><CircularProgress />Loading...</div>;
    } else {
      return (
      <>
        <Typography sx={{ my: 5, mx: 2 }} color="text.secondary" align="center">
            Search for a Client
        </Typography>
        <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
          <AppBar
          position="static"
          color="default"
          elevation={0}
          sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
          >
            <Toolbar>
              <Grid container spacing={3} alignItems="center">
                <Grid item>
                  <SearchIcon color="inherit" sx={{ display: 'block' }} />
                </Grid>
                <Grid item xs>
                  <TextField
                    fullWidth
                    placeholder="Search by name or email address"
                    onChange = {handleChange}
                    InputProps={{
                        disableUnderline: true,
                        sx: { fontSize: 'default' },
                    }}
                    value={val}
                    variant="standard"
                  />
                </Grid>
                  <Grid item>
                    <Tooltip title="Refresh">
                      <IconButton onClick={() => clearSearch()}>
                        <RefreshIcon color="inherit" sx={{ display: 'block' }} />
                      </IconButton>
                    </Tooltip>
                  </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <Typography sx={{ my: 5, mx: 2 }} color="text.secondary" align="center">
            {filteredClients.length === 1 ? `${filteredClients.length} match found` : `${filteredClients.length} matches found`}
          </Typography>
            <Grid container>
              {searchList()}
            </Grid>
        </Paper>
      </>
      );
    }
  }