module.exports = (app) => {
    const clients = require('../controllers/client.controller.js');

    // Add a new Client
    app.post('/clients', clients.create);

    // Retrieve all clients
    app.get('/clients', clients.findAll);

    // Retrieve an individual Client with clientId
    app.get('/clients/:clientId', clients.findOne);

    // Update a Client with clientId
    app.put('/clients/:clientId', clients.update);

    // Delete a Client with clientId
    app.delete('/clients/:clientId', clients.delete);
}