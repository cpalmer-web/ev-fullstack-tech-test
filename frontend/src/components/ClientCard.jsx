import React from 'react';
import Link from '@mui/material/Link';
import moment from 'moment'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Typography from '@mui/material/Typography';
import { Avatar } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import { removeClient } from '../utils'

const ClientCard = ({client}) => {

    const removeClientReq = async (id) => {
        const data = removeClient(id)
        .then((res) => res.json())
        .then((json) => {
          setMessage(json.message)
        })
      }

    return (       
      <Card>  
        <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: '#009ce5' }} aria-label="recipe">
            {client.name.charAt(0)}
          </Avatar>
        }
        title={`${client.name}`}
        subheader={`${moment(client.createdAt).format("MM ddd, YYYY")}`}
        />
        <CardContent>
          <Typography color="textSecondary" variant="body">
              {client.email}
          </Typography>
        </CardContent>
        <CardContent>
          <Typography color="textSecondary" variant="body">
              {client.companyFields}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
        <Link onClick={() => removeClientReq(client._id)} >
          <IconButton aria-label="remove">
            <GroupRemoveIcon />
          </IconButton>
        </Link>
        </CardActions>
      </Card>        
    );    
}

export default ClientCard;